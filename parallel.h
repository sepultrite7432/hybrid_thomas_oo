#ifndef _PARALLEL_H_
#define _PARALLEL_H_

#include <mpi.h>
#include <omp.h>

class MPI													//Abbreviation for Message-Passing Interface
{
	private:
	
		int mpi_rank_						; 		//Rank of a process in MPI_COMM_WORLD
		int mpi_size_						; 		//Size of the MPI_COMM_WORLD communicator
		MPI_Comm mpi_old_comm_	;			//Initial communicator MPI_COMM_WORLD
		
	public:
	
		void Initialize()						;
		int  GetProcessCount() const;
		int  GetCommRank() const 		;
		MPI_Comm GetComm() const		; 
		void Finalize()							;
		bool IOProcessor() const		;   
		
};

#endif