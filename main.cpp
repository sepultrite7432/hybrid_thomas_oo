/*====================================================================================*/
/* PROGRAM: MPI + OpenMP implementation of the modified parallel Thomas Algorithm 		*/
/*====================================================================================*/
/* for solving tridiagonal system/s of equations. Each MPI process contains a subset	*/
/* of equations of all the linear systems. Multiple OpenMP threads within the MPI 		*/
/* process solve these local partial systems using a modified Thomas algorithm Forward*/
/* and Backward pass. This step expresses the local equations in terms of the first 	*/
/* and last local variable. The first and the last local equations from each process  */
/* are collected onto a single process and solved using the traditional serial Thomas */
/* algorithm. The solutions of these first and last variables (for all the systems) 	*/
/* is then transferred back to the respective processes from which they came. 				*/
/* Since the local equations are in terms of the (now solved) first and last local    */
/* variables, they can be solved. 																										*/
/*====================================================================================*/
/* PROGRAM AUTHOR: Gaurav Saxena 																											*/
/* BASED ON: PaScaL_TDMA: A library of parallel and scalable solvers for massive 			*/
/* tridiagonal systems - Kim KH, Kang JH, Pan X, Choi JI 															*/
/*====================================================================================*/

#include <iostream>

#include "thomas.h"
#include "parallel.h"

using namespace std; 

int main(int argc, char *argv[])
{
	MPI 		mpi; 
	Thomas 	thomas;
	double start_time, end_time;  

	mpi.Initialize();
	
	if(mpi.IOProcessor())
	{
		std::cout<<"=>Initialized MPI for MPI+OpenMP Thomas algorithm"<<std::endl;
		std::cout<<"	Total MPI processes = "<<mpi.GetProcessCount()<<std::endl;  
	}
	
	thomas.SetInput(argc, atoi(argv[1]), atoi(argv[2]), mpi);
	
	if(mpi.IOProcessor())
	{
		std::cout<<"=>Set total linear systems and equations per system for Thomas algorithm"<<std::endl;
		std::cout<<"	Total linear systems = "<<thomas.GetNoSystems()<<std::endl;
		std::cout<<"	Total Equations per system = "<<thomas.GetEqsPerSystem()<<std::endl; 
	}
	
	if(mpi.IOProcessor())
		std::cout<<"=>Allocating memory for super/sub/main diagonal elements on Tridiagonal matrix"<<std::endl; 
	
	thomas.Initialize(mpi); 
	
	if(mpi.IOProcessor())
		std::cout<<"=>Starting parallel Thomas solve of the Tridiagonal matrix"<<std::endl;
	
	MPI_Barrier(mpi.GetComm());
	start_time = MPI_Wtime();
	thomas.Solve(mpi);
	MPI_Barrier(mpi.GetComm()); 
	end_time = MPI_Wtime(); 
	
	if(mpi.IOProcessor())
		std::cout<<"	Total time taken to solve = "<< end_time - start_time <<" seconds"<<std::endl; 
		
	thomas.DisplaySolution(mpi); 

	
	mpi.Finalize();  
	
 
}