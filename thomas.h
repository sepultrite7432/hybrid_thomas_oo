/*--------------------------------------------------------------------------*/
/* Thomas.h declares Tridiagonal matrix sub/main/super diagonals and rhs 		*/
/* The final solution will be in rhs vector 																*/
/*  and hence no separate solution vector is declared 											*/															
/*--------------------------------------------------------------------------*/

#ifndef _THOMAS_H_						
#define _THOMAS_H_

#include <vector>
#include "parallel.h"

class MPI; 

class Thomas
{
	private:
	
		std::vector<std::vector<double>> sub_diag_	;
		std::vector<std::vector<double>> main_diag_	; 	
		std::vector<std::vector<double>> sup_diag_	; 
		std::vector<std::vector<double>> rhs_				; 
		int no_of_sys_															; 
		int eqs_per_sys_														;
		int eqs_psps_																;	//Equations per system per process
		
		std::vector<double> red_abcd_ 							; //Contains all coeffs. of 1st and last reduced local Eqs.
		std::vector<double> all_red_abcd_						; //All reduced Eqs. coefficients from all processes on 'chosen' process
		std::vector<std::vector<double>> red_a_			; //Sub-diagonal reduced Eqs. coeffs. on 'chosen' process
		std::vector<std::vector<double>> red_b_			;	//Main-diagonal reduced Eqs. coeffs. on 'chosen' process
		std::vector<std::vector<double>> red_c_			; //Super-diagonal reduced Eqs. coeffs. on 'chosen' process
		std::vector<std::vector<double>> red_d_			; //rhs from reduced Eqs. on 'chosen' process
		
	public:
		
		void SetInput(int no_of_inputs, int systems, int equations, MPI &mpi); 
		int GetNoSystems() const								 							 							 ;
		int GetEqsPerSystem()	const 						 							 							 ; 
		void Initialize(MPI &mpi)								 							 							 ;
		void Solve(MPI &mpi)										 							 							 ;
		void ForwardBackwardPass()							 							 							 ;
		void GatherReducedEquations(MPI &mpi, int chosen_rank) 							 ;
		void SolveReducedEquations(MPI &mpi, int chosen_rank)								 ;
		void ScatterReducedSolutions(MPI &mpi, int chosen_rank)							 ;
		void SolveModifiedOriginalEquations()																 ;
		void DisplaySolution(MPI &mpi)																			 ;    
		  
};

#endif