PROGRAM_NAME := main_thomas.exe

CC := mpic++
CFLAGS :=  -O2 -fopenmp -g -std=c++11
 

COMPILE_COMMAND := $(CC) $(CFLAGS) 

Thomas_OBJECTS:= thomas.o 
Parallel_OBJECTS:= parallel.o 
ALL_OBJECTS := $(Thomas_OBJECTS) $(Parallel_OBJECTS)

# compile the project 

all: main.cpp $(ALL_OBJECTS)
	$(COMPILE_COMMAND) -o $(PROGRAM_NAME) $(ALL_OBJECTS) main.cpp 

thomas.o: thomas.cpp
	$(COMPILE_COMMAND) -c ./thomas.cpp

parallel.o: parallel.cpp
	$(COMPILE_COMMAND) -c ./parallel.cpp
	
# cleanup

clean:
	rm -f *.o
	rm -f $(PROGRAM_NAME)*

