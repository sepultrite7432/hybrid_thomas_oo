#include "parallel.h"

void MPI::Initialize()
{	
	int provided_level{};								//Check returned MPI level
	mpi_old_comm_ = MPI_COMM_WORLD; 		//A handle to the communicator
	 
	MPI_Init_thread(NULL, NULL, MPI_THREAD_FUNNELED, &provided_level);
	if(provided_level != MPI_THREAD_FUNNELED)
		MPI_Abort(mpi_old_comm_, -1);
		
	MPI_Comm_size(mpi_old_comm_, &mpi_size_);		//Size of initial communicator 
	MPI_Comm_rank(mpi_old_comm_, &mpi_rank_);	  //Rank of each process 
}

int  MPI::GetProcessCount() const
{
	return mpi_size_ ; 
}

int  MPI::GetCommRank() const 
{
	return mpi_rank_ ;
}

MPI_Comm MPI::GetComm() const
{
	return mpi_old_comm_ ; 
}

void MPI::Finalize()
{
	MPI_Finalize(); 
}

bool MPI::IOProcessor() const
{
 	bool flag = (0 == mpi_rank_ ? true : false); 
 	return flag; 
}