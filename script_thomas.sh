#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=1
#SBATCH -t 01:00:00
#SBATCH -o output-%j
#SBATCH -e error-%j
#SBATCH --exclusive

#-----------------------------------------------------------------------------------
# Better to use OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK, this way will only need to
# change --cpus-per-task=<no_of_threads>
#-----------------------------------------------------------------------------------

export OMP_DISPLAY_ENV=true
export OMP_SCHEDULE=STATIC
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=spread
export OMP_PLACES=threads

#export OMP_PLACES="{0:1}:48:1"
#export OMP_PLACES='cores(48)'

#--------------------------------------------
# Simplest Execution, can be used for testing
#--------------------------------------------

 mpiexec ./main_thomas.exe 1 40

#-------------------------------------------------------------
# Better to use --map-by ppr syntax when measuring performance
# MN4, best configuration is 1 MPI process per socket and
# 24 OpenMP threads per MPI process.
# (This is because we have 2 sockets with 24 cores each)
#-------------------------------------------------------------

# mpiexec --map-by ppr:1:socket:pe=$OMP_NUM_THREADS  --report-bindings ./main_thomas.exe

#--------------------------------------------------------------------------------------
# Uncomment if using DDT for debugging (1) if mpiexec doesn't connect then use (2) srun
#--------------------------------------------------------------------------------------

# ddt --connect mpiexec ./main_thomas.exe
# ddt --connect srun ./main_thomas.exe


#----------------------------------------------------------
# Using OpenMPI binding policies (non ppr execution syntax)
#----------------------------------------------------------

#mpiexec --map-by socket 	--bind-to core  --report-bindings ./main_thomas.exe  
#mpiexec --map-by node 		--bind-to none 	--report-bindings ./examples/main_thomas.exe 
