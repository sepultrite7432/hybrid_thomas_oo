#include<iostream>
#include "thomas.h"

using namespace std; 

void Thomas::SetInput(int no_of_inputs, int systems, int equations, MPI &mpi)
{

	//Need to display error message if arguments is != 3
	
	if(systems <= 0 || equations <=0 )
	{
		std::cout<<"A <=0 value for the no. of linear systems or equations is not allowed"<<std::endl;
		exit(1); 
	}	
	else
	{ 
		no_of_sys_ 	 = systems;
		eqs_per_sys_ = equations; 
	}
}

int Thomas::GetNoSystems() const
{
	return no_of_sys_ ; 
}

int Thomas::GetEqsPerSystem()	const 
{
	return eqs_per_sys_ ; 
}

void Thomas::Initialize(MPI &mpi)
{
		int number_of_processes = mpi.GetProcessCount();
		int rank_of_process			= mpi.GetCommRank();  
		
		eqs_psps_ = eqs_per_sys_ / number_of_processes; 
		
		sup_diag_.resize(	no_of_sys_, vector<double>(eqs_psps_ + 2));
		main_diag_.resize(no_of_sys_, vector<double>(eqs_psps_ + 2));
		sub_diag_.resize(	no_of_sys_, vector<double>(eqs_psps_ + 2));
		rhs_.resize(			no_of_sys_, vector<double>(eqs_psps_ + 2));
		
		
		#pragma omp parallel for default(none) \
						shared(sup_diag_, main_diag_, sub_diag_, rhs_, no_of_sys_, eqs_psps_) \
						schedule(static)
		for(int i = 0; i < no_of_sys_ ; i++)
		{
			for(int j = 1; j <= eqs_psps_ ; j++)
			{
				sup_diag_[i][j]	 = -1.0; 
				main_diag_[i][j] =  2.0;
				sub_diag_[i][j]  = -1.0;
				rhs_[i][j] 			 =  1.0;  
			}
		}
		
		if(rank_of_process == 0)
		{
			#pragma omp parallel for default(none) shared(sub_diag_, no_of_sys_) schedule(static)
			for(int i = 0; i < no_of_sys_ ; i++)
				sub_diag_[i][1] = 0.0; 
		} 
		
		if(rank_of_process == number_of_processes-1)
		{
			#pragma omp parallel for default(none) shared(sup_diag_, no_of_sys_, eqs_psps_) schedule(static)
			for(int i = 0; i < no_of_sys_ ; i++)
				sup_diag_[i][eqs_psps_] = 0.0; 
		} 		
}

void Thomas::Solve(MPI &mpi)
{
	int chosen_rank {0}  ;
	 
	ForwardBackwardPass();
	GatherReducedEquations(mpi, chosen_rank); 
	SolveReducedEquations(mpi, chosen_rank); 
	ScatterReducedSolutions(mpi, chosen_rank);
	SolveModifiedOriginalEquations();   
}

void Thomas::ForwardBackwardPass()
{

#pragma omp parallel for default(none) shared(sub_diag_, main_diag_, sup_diag_, rhs_, no_of_sys_, eqs_psps_) schedule(static)
for(int i = 0; i <= no_of_sys_-1; i++)
{

/*---------------------------------------------------------*/ 
/* Step-1(a) Forward pass of the Modified Thomas Algorithm */
/* Elimination of lower diagonal elements 								 */ 
/*---------------------------------------------------------*/

	sub_diag_[i][1] = sub_diag_[i][1]/main_diag_[i][1];
	sup_diag_[i][1] = sup_diag_[i][1]/main_diag_[i][1];
	rhs_[i][1] 			= rhs_[i][1]/main_diag_[i][1];

	sub_diag_[i][2]  = sub_diag_[i][2]/main_diag_[i][2];
	sup_diag_[i][2]  = sup_diag_[i][2]/main_diag_[i][2];
	rhs_[i][2]       = rhs_[i][2]/main_diag_[i][2];
	
	for(int j = 3; j <= eqs_psps_; j++)														//Start from third equation 
		{
			double r = 1.0 / ( main_diag_[i][j] - sub_diag_[i][j] * sup_diag_[i][j-1] );
			
			rhs_[i][j]  		 = r * ( rhs_[i][j] - sub_diag_[i][j] * rhs_[i][j-1] );
			sup_diag_[i][j]  = r * sup_diag_[i][j];
			sub_diag_[i][j]  = -1.0 * r * sub_diag_[i][j] * sub_diag_[i][j-1];  
		}
	
/*----------------------------------------------------------*/ 
/* Step-1(b) Backward pass of the Modified Thomas Algorithm */ 
/* Elimination of Upper Diagonal matrices 									*/
/*----------------------------------------------------------*/		

	for(int j = eqs_psps_-2; j >= 2; j--)												//Start from third last equation
	{
		rhs_[i][j] = rhs_[i][j] - sup_diag_[i][j] * rhs_[i][j+1];  
		sub_diag_[i][j] = sub_diag_[i][j] - sup_diag_[i][j] * sub_diag_[i][j+1]; 	//REMEMBER: use c[i][j] BEFORE it is updated ! 
		sup_diag_[i][j] = -1.0 * sup_diag_[i][j] * sup_diag_[i][j+1];
	}
	
/*------------------------------------------------------------------------------------------------------*/	
/* Till now all the b's should have become = 1 (although we have not made the 'b' array explicitly = 1) */	
/* The expressions for 'r' and 'd' are wrong in the paper - consult the Fortran program 								*/
/*------------------------------------------------------------------------------------------------------*/	

	double r = 1.0 / ( 1.0 - sub_diag_[i][2] * sup_diag_[i][1] );							//r=1.0/(b[i][2]-a[i][2]*c[i][1] but b[i][2]=1.0 now)
	
	rhs_[i][1]  = r * (rhs_[i][1] - sup_diag_[i][1] * rhs_[i][2]);							//This was also incorrect in the paper
	sup_diag_[i][1]  = -1.0 * r * sup_diag_[i][1] * sup_diag_[i][2];
	sub_diag_[i][1]  = r * sub_diag_[i][1];  
}

}

void Thomas::GatherReducedEquations(MPI &mpi, int chosen_rank)
{
 
/*-------------------------------------------------------------------------------------------------------------------------------------------*/ 
/* Storage sequence of coefficients : (0th system first) a1 b1 c1 d1 a2 b2 c2 d2 (1st system second) a3 b3 c3 d3 a4 b4 c4 d4 (3rd system) ...*/
/*-------------------------------------------------------------------------------------------------------------------------------------------*/ 

red_abcd_.resize(no_of_sys_ * 2 * 4); 	//Each system contributes 2 equations, coefficients in each eq = 4
#pragma omp parallel default(none) shared(red_abcd_, sub_diag_, sup_diag_, rhs_, eqs_psps_) 
{
 int tid = omp_get_thread_num();
 int work_chunk = no_of_sys_ /omp_get_num_threads();
 int counter = tid * work_chunk * 2 * 4; 
 
	#pragma omp for schedule(static)
 		for(int i = 0; i <= no_of_sys_-1; i++)
 		{
 			red_abcd_[counter++] = sub_diag_[i][1]; 
 			red_abcd_[counter++] = 1.0; 									//all b's (main diagonal) are 1 
 			red_abcd_[counter++] = sup_diag_[i][1];
 			red_abcd_[counter++] = rhs_[i][1];
 	
 			red_abcd_[counter++] = sub_diag_[i][eqs_psps_];
 			red_abcd_[counter++] = 1.0; 									//all b's (main diagonal) are 1
 			red_abcd_[counter++] = sup_diag_[i][eqs_psps_];
 			red_abcd_[counter++] = rhs_[i][eqs_psps_];
 		}
}	
	
/*---------------------------------------------------------------------------------------*/
/* Choose a rank at which we will solve these reduced systems of Equations 							 */
/* Then declare an array big enough on this rank to contain these coefficients and 			 */
/* put them in the right format i.e. a, b, c, d <--- easier to apply Thomas solver 			 */
/* Later optimization: Distribute systems on multiple processes so that different systems*/
/* can be solved in parallel - this will be a new thing and we keep multiple cores busy  */
/*---------------------------------------------------------------------------------------*/	
 
 
 if(mpi.GetCommRank() == chosen_rank)
 {
 	all_red_abcd_.resize(mpi.GetProcessCount() * no_of_sys_ * 2 * 4);				//Used in MPI_Gather to receive data
 	
 	red_a_.resize(no_of_sys_, vector<double>(2 * mpi.GetProcessCount()));
 	red_b_.resize(no_of_sys_, vector<double>(2 * mpi.GetProcessCount()));
 	red_c_.resize(no_of_sys_, vector<double>(2 * mpi.GetProcessCount()));
 	red_d_.resize(no_of_sys_, vector<double>(2 * mpi.GetProcessCount()));
 }

 	MPI_Gather(&red_abcd_[0], no_of_sys_ * 2 * 4, MPI_DOUBLE, &all_red_abcd_[0], no_of_sys_ * 2 * 4 , MPI_DOUBLE, chosen_rank, mpi.GetComm());

 if(mpi.GetCommRank() == chosen_rank)
 {
 	int size = mpi.GetProcessCount(); 
 	#pragma omp parallel for default(none) shared(red_a_, red_b_, red_c_, red_d_, all_red_abcd_, size) schedule(static)
 		for(int i = 0; i <= no_of_sys_-1; i++)
 		{
 			int counter = i * 8; 
 			for(int j = 0; j <= 2 * size - 1; j = j + 2)
 			{
 				red_a_[i][j] = all_red_abcd_[counter++];
 				red_b_[i][j] = all_red_abcd_[counter++];
 				red_c_[i][j] = all_red_abcd_[counter++];
 				red_d_[i][j] = all_red_abcd_[counter++];
 			
 				red_a_[i][j+1] = all_red_abcd_[counter++];
 				red_b_[i][j+1] = all_red_abcd_[counter++];
 				red_c_[i][j+1] = all_red_abcd_[counter++];
 				red_d_[i][j+1] = all_red_abcd_[counter++];
 			
 				counter = counter + (no_of_sys_-1) * 2 * 4; 			 
 			}
 		}
 	}
 }
 
void Thomas::SolveReducedEquations(MPI &mpi, int chosen_rank)
{
 /*------------------------------------------------------------------------------------*/
 /* Serial Thomas algorithm on "chosen_rank" to solve the reduced systems of Equations */
 /*------------------------------------------------------------------------------------*/

	if(mpi.GetCommRank() == chosen_rank)
	{
		int mpi_size = mpi.GetProcessCount();
		#pragma omp parallel for default(none) shared(red_d_, red_c_, red_b_, red_a_, mpi_size) schedule(static)
		 for(int i = 0; i <= no_of_sys_-1; i++)
		 {
		 	red_d_[i][0] = red_d_[i][0]/red_b_[i][0];   	//b[i][whatever]=1 for all the equations so later change to 1.0
		 	red_c_[i][0] = red_c_[i][0]/red_b_[i][0]; 	  //Similarly no need for this step 
		 	
		 	for(int j = 1; j <=2 * mpi_size-1; j++)
		 	{
		 		double r = 1.0 / ( red_b_[i][j] - red_a_[i][j] * red_c_[i][j-1] );
		 		red_d_[i][j]  = r * ( red_d_[i][j] - red_a_[i][j] * red_d_[i][j-1] );
		 		red_c_[i][j]  = r * red_c_[i][j]; 
		 	}
		 }
		 
		#pragma omp parallel for default(none) shared(red_d_, red_c_, mpi_size) schedule(static) 
		 for(int i = 0; i <= no_of_sys_-1; i++)
		 {
		    for(int j = 2 * mpi_size-2; j >= 0; j--)
		    {
		    	red_d_[i][j] = red_d_[i][j] - red_c_[i][j] * red_d_[i][j+1]; 
		    }
		 } 
	 }
}
 
void Thomas::ScatterReducedSolutions(MPI &mpi, int chosen_rank)
{

/*-----------------------------------------------------------------------------------------------------------------------------*/  
/* Now the solutions of reduced system of equations is in red_d[][] arrays. 							 																		 */
/* red_d[0<=i<=TOTAL_VOXELS_Y][0] and red_d[0<=i<=TOTAL_VOXELS_Y][1] - should go to rank 0 																		 */
/* red_d[0<=i<=TOTAL_VOXELS_Y][2] and red_d[0<=i<=TOTAL_VOXELS_Y][3] - should go to rank 1 																		 */
/* red_d[0<=i<=TOTAL_VOXELS_Y][4] and red_d[0<=i<=TOTAL_VOXELS_Y][5] - should go to rank 2 																		 */
/* ... 																																										 																		 */
/* red_d[0<=i<=TOTAL_VOXELS_Y][2 * mpi_Size-2] and red_d[0<=i<=TOTAL_VOXELS_Y][2 * mpi_Size-1] - should go to rank (mpi_Size-1)*/	
/*-----------------------------------------------------------------------------------------------------------------------------*/	

  std::vector<double> contig_red_d;				//Declaration on all but allocation only on chosen_rank 
  std::vector<double> recv_red_d  ;
  int counter {0}									;  
  int mpi_size = mpi.GetProcessCount(); 
  
  if(mpi.GetCommRank() == chosen_rank)
  {
  	contig_red_d.resize(no_of_sys_ * 2 * mpi_size);
  	int iterations = 0;
  	int cumulative_written = 0; 
  	 
  	for(int j = 0; j <= 2*mpi_size-1; j = j+2)
  	{ 
  		#pragma omp parallel for default(none) shared(contig_red_d, red_d_, j) firstprivate(cumulative_written) schedule(static)
  		for(int i = 0; i <= no_of_sys_-1; i++)
  		{
  			int counter = i * 2 + cumulative_written; 
  			contig_red_d[counter++] = red_d_[i][j];
  			contig_red_d[counter++] = red_d_[i][j+1];
  		}
  		iterations++;
  		cumulative_written = iterations * no_of_sys_ * 2;  
  	}
  }
  
/*---------------------------------------------------------------------------------------------------------*/
/* Declare the buffer to contain the incoming solutions on each process 																	 */
/* Length of this buffer is TOTAL_VOXELS_Y * 2 i.e. Each process gets 2 solved variables per linear system */
/*---------------------------------------------------------------------------------------------------------*/

  recv_red_d.resize(no_of_sys_ * 2); 
  
  MPI_Scatter(&contig_red_d[0], no_of_sys_ * 2, MPI_DOUBLE, &recv_red_d[0], no_of_sys_ * 2, MPI_DOUBLE, chosen_rank, mpi.GetComm()); 
  
/*----------------------------------------------------------------------------------------------*/
/* Now put the received solutions of the first and last local equation in the local d_[][] array */
/*----------------------------------------------------------------------------------------------*/
	
	counter = 0;
	#pragma omp parallel for default(none) shared(rhs_, recv_red_d, eqs_psps_) private(counter) schedule(static)
		for(int i=0; i <= no_of_sys_-1; i++)
		{
			counter = i * 2; 
			rhs_[i][1] 					= recv_red_d[counter++];
			rhs_[i][eqs_psps_] 	= recv_red_d[counter++]; 
		}		 
}

void Thomas::SolveModifiedOriginalEquations()
{
/*----------------------------------------------------------------------------------------------------------------*/	
/* Now the solutions to the first and last local variable on each process is in rhs_[][1] and rhs_[][x_voxels_pp] */
/* Use this on every process to solve the local 2nd to 2nd last variable 																					*/
/* All the solutions then are in 'd' array 																																				*/
/*----------------------------------------------------------------------------------------------------------------*/

 #pragma omp parallel for default(none) shared(rhs_, sub_diag_, sup_diag_, eqs_psps_) schedule(static)
 for(int i = 0; i <= no_of_sys_-1; i++)
 {
 	for(int j = 2; j <= eqs_psps_-1; j++)
 	{
 		rhs_[i][j] = rhs_[i][j] - sub_diag_[i][j] * rhs_[i][1] - sup_diag_[i][j] * rhs_[i][eqs_psps_]; 
 	}
 }
}

void Thomas::DisplaySolution(MPI &mpi)
{
/*-------------------------------------------------------------------------------------------------------*/ 
/* To see the final solution for Linear System 'i', print all rhs_[i][1 <=j<= eqs_psps_] on each process */
/*-------------------------------------------------------------------------------------------------------*/
 
  for(int lin_sys_num = 0; lin_sys_num <= no_of_sys_-1; lin_sys_num++)
  {
  	if(mpi.GetCommRank() == 0)
  		std::cout<<"SYSTEM:"<<lin_sys_num<<std::endl; 
  			
  	for(int rnk_ctr = 0; rnk_ctr <= mpi.GetProcessCount()-1; rnk_ctr++)
  	{
  		if(rnk_ctr == mpi.GetCommRank())
  		{
  			for(int i = 1; i <= eqs_psps_; i++)
  				std::cout<<rhs_[lin_sys_num][i]<<" ";
  				
  			std::cout<<std::endl;  
  		}
  		MPI_Barrier(mpi.GetComm()); 
  	}
  }
}
							 


